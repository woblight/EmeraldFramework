# EmeraldFramework
Framework for World of Warcraft

## About
It aims to provide a set of both basic and complex elements to create dynamic UI structures while abstracting completely from default api. Purely written in lua and doesn't support XML.

**[Documentation](https://woblight.gitlab.io/EmeraldFramework)**

## Status: Beta
Currently under development, mostly stable.

## Current Features
- Property system
- Basic elements (Rectangles, Buttons, Labels, ...)
- Basic Layouts (not fully implemented yet)
- Basic Windows
- Native frames recycling

# Class Object

Constructors
------------------------------------------------

1. `(parent)`

### Arguments
- `1.`:
    * `parent`: parent object. Must be an `Object` or its derivate.
    
### Description
- `1.`:  
    Creates an `Object` having `parent` as parent.


`:connect(signal, fun)`
------------------------------------------------

### Arguments
- `signal`: signal name
- `fun`: the callback function

### Returns
- a `Connection` between the signal and `fun` which can be stored to disconnect `fun` later.

### Description

Connects the signal to `fun`, causing `fun` to be called whenever the `Signal` is called until its disconnection.


`:connect(signal, target, functionName)`
------------------------------------------------

### Arguments
- `signal`: signal name
- `target`: an `Object` or derivate
- `functionName`: a string containing the name of the function to be called

### Returns
- a `Connection` between the signal and `fun` which can be stored to disconnect later.

### Description

Connects the signal to `target`'s `functionName` member function, causing `target[functionName]` to be called whenever the `Signal` is called until its disconnection.


`:disconnect(signal, fun)`
------------------------------------------------

`:disconnect(signal, target, functionName)`
------------------------------------------------

### Description

Disconnect matching connections.

`:disconnectAll(signal)`
------------------------------------------------

### Description

Disconnect all connections.

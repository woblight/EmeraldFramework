# Class Connection

`:disconnect()`
------------------------------------------------

### Returns
- `nil`

### Description

Disconnects the connection.

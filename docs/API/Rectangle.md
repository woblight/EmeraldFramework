# Class Rectangle

**Inherits [Item](Item.md)**

Display a rectangle with or without a border.

# Property Documentation

## color

An {r,g,b\[,a\]} table (values in \[0,1\]) representing the color of the rectangle.

## borderColor

An {r,g,b\[,a\]} table (values in \[0,1\]) representing the color of the border of the rectangle.

## borderWidth

The width of the border. The border expands inwards (does not exceed width/height of the rectangle).

## layer

The layer of the rectangle's native textures.

## blendMode

The blend mode of the rectangle's native textures.

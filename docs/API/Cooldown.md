# Class Cooldown

**Inherits [Item](Item.md)**

Display a cooldown animation.

# Property Documentation

## duration

The duration of the cooldown in seconds.

## start

The timestamp of the beginning of the cooldown.

## enabled

Whether the cooldown is enabled.

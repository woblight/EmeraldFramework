# Class Signal

`__call__(...)`
------------------------------------------------

### Description

Iterates over its connections and calls them forwarding the arguments.
